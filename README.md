# Iterative Paralog Algorithm
Matching strategy for building a coupled MSA of two interacting protein families, by iterative pseudo coevolution maximization.

Based on Bitbol et al, PNAS, 2017

# Compilation

If the Intel C++ compilers with MKL are present, matrix inversion is done using the fast MKL routines.
Otherwise, the inversion relies on contained Alglib-3.16.0 linear algebra routines.

To compile using Intel+KML, use the makfeiles/Makefile.intel for compilation.

Use 
```shell
make
```
for standard compilation or 

```shell
make -f makefiles/Makefile.intel
```
for Intel compilation.

The executable is located in build/opt/ipa.

# Usage
Type ipa -h for command line options.

```shell
Usage: lbsDCA [options]
       -f1    : 1st Fasta file
       -f2    : 2st Fasta file
       -os1   : 1st Organism file
       -os2   : 2st Organism file
       -s     : Random number seed [default time]
       -Ni    : N increment [default 6]
       -seedMSA  : Seed of matched MSA [default none]
       -t     : Number of threads [default 4]
       -o     : Output Prefix [default "output"]
```


