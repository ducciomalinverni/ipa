#ifndef DEF_IPA
#define DEF_IPA

#include "FastaHandler.h"
#include "InputHandler.h"
#include <limits>


#if defined(__INTEL_COMPILER)
#    include <mkl.h>
#else
#include "stdafx.h"
#include "linalg.h"
#endif

void runIPA(unsigned  int Nincrement,MSA seedMSA, unsigned int B,
	    MSA msa1, MSA msa2,unsigned int N1, unsigned int N2,
	    unsigned int B1, unsigned int B2, OrgsIndexes idx1, OrgsIndexes idx2,Ids ids1, Ids ids2,
	    Organisms sharedOrgs,std::string OutputPrefix);


MSA buildRandomSeed(Organisms sharedOrganisms,MSA msa1,MSA msa2,unsigned int N1,unsigned int N2,
		    OrgsIndexes idx1, OrgsIndexes idx2,unsigned int& Bseed,unsigned int rngSeed);

void buildCovarianceMatrix(double* C, MSA currentMSA, unsigned int B, unsigned int N,
			   double pseudoCounts, double reweightMaxId, double* weights);

void computeCouplingsInIsingGauge(double* J, double* Cred, unsigned int N);

void computeMatchingScores(double* J, double* pairs,unsigned int* pairIndexes, MSA msa1, MSA msa2,unsigned int N1, unsigned int N2,
			   OrgsIndexes idx1, OrgsIndexes idx2,Organisms sharedOrganisms);

void selectedBestMatchingPairs(MSA currentMSA);
std::vector<std::pair<unsigned int,double> > buildNextIterationMSA(unsigned int Nincrement,
								   double* pairs,unsigned int nPairs, MSA msa1,MSA msa2,
								   unsigned int N1, unsigned int N2, MSA currentMSA);

double findMinEnergy(std::vector<std::vector<double> > Es,unsigned int &i,unsigned int &j,double &Emin,double &Emin2);
void getNLowerEnergies(std::vector<std::vector<double> > Es,unsigned int i, unsigned int j,
			       unsigned int &n1, unsigned int &n2);
#endif
