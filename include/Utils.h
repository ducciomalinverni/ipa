#ifndef DEF_UTILS
#define DEF_UTILS

#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <cmath> 
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <numeric>
#include <omp.h>

typedef std::vector<std::string> Organisms;
typedef std::vector<std::string> Ids;
typedef std::vector<std::vector<unsigned int> > OrgsIndexes;
Organisms unique(Organisms vec);
bool comparator ( const std::pair<unsigned int,double> & l, const std::pair<unsigned int, double>& r);
std::vector<std::string> split(const std::string &s, char delim);

#endif
