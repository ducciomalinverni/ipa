#ifndef DEF_FASTAHANDLER
#define DEF_FASTAHANDLER

#include "Utils.h"

typedef unsigned int* MSA;
typedef std::vector<unsigned int> Sequence; 

MSA readFasta(std::string fastaFile, unsigned int &N, unsigned int &B,Ids &ids);
unsigned int AAtoNumber(char AA);
char numberToAA(unsigned int AA);
void writePairedFasta(unsigned int nPairs,MSA msa1, MSA msa2,unsigned int N1, unsigned int N2,
		      Ids ids1, Ids ids2, double* pairs,std::vector<std::pair<unsigned int,double> > &scores,
		      std::string fileName);
#endif
