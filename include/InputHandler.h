#ifndef DEF_INPUTHANDLER
#define DEF_INPUTHANDLER

#include "Utils.h"
#include "FastaHandler.h"

struct Parameters{
  unsigned int Ninc;
  std::string msa1File;
  std::string msa2File;
  std::string os1File;
  std::string os2File;
  std::string seedFile;
  unsigned int seed;
  unsigned int nThreads;
  std::string outFile;
};

Parameters parseInput(int argc, char** argv);
double computeSeqIdWeights(double* weights, MSA msa,unsigned int N, unsigned int B, double maxId);
Organisms readOrganisms(std::string organismFile);
Organisms filterBySharedOrganisms(MSA &msa1,MSA &msa2, Organisms &os1, Organisms &os2,
				  unsigned int N1,unsigned int N2,unsigned int &B1, unsigned int &B2,
				  Ids &ids1, Ids &ids2);
OrgsIndexes getOrgsIndexes(Organisms orgs, Organisms sharedOrgs);

#endif
