#include "Utils.h"

Organisms unique(Organisms vec){
  Organisms v=vec;
  sort(v.begin(),v.end());
  auto last=unique(v.begin(),v.end());
  v.erase(last,v.end());
  return v;
}

bool comparator ( const std::pair<unsigned int,double> & l, const std::pair<unsigned int, double>& r){
  return l.second > r.second;
}


void split(const std::string &s, char delim, std::vector<std::string> &elems) {
  std::stringstream ss;
  ss.str(s);
  std::string item;
  while (std::getline(ss, item, delim)) {
    elems.push_back(item);
  }
}

std::vector<std::string> split(const std::string &s, char delim) {
  std::vector<std::string> elems;
  split(s, delim, elems);
  return elems;
}
