#include "FastaHandler.h"

/* Reads a fasta file, input as string, and return an unsigned int array containing the amino acids sequences.
The array is accessed as msa[b][i]= msa[b*N+i]. Also returns the dimensions B (number of sequences) 
and N (number of nodes) of the msa
*/
MSA readFasta(std::string fastaFile,unsigned int &N, unsigned int &B,Ids &ids){

  std::ifstream fastaStream(fastaFile.c_str());
  MSA msaFull;
  std::vector<Sequence> msa;
  std::string line;
  unsigned int b=-1;
  
  while(!fastaStream.eof()){
    fastaStream >> line;
    if(line[0]=='>'){
      msa.push_back(Sequence(0));
      b++;
      std::vector<std::string> splitLine=split(line,'|');
      if(splitLine.size()==3)
	ids.push_back(splitLine[1]);
      else if(splitLine.size()==2)
	ids.push_back(splitLine[1]);
      else
	ids.push_back(line);
      
      std::getline(fastaStream,line);
    }
   else
      for(unsigned int i=0;i<line.size();i++)
	msa[b].push_back(AAtoNumber(line[i]));
  }
  fastaStream.close();
  
  B=msa.size();
  N=msa[0].size();
  
  msaFull=new unsigned int[B*N];
  for(unsigned int i=0;i<B;i++)
    for(unsigned int j=0;j<N;j++)
      msaFull[i*N+j]=msa[i][j];
  return msaFull;
}

/* Converts amino acid characters to numerical form, using the alphabetical ordering*/
unsigned int AAtoNumber(char AA){

  switch(AA){
    case '-': return 0;
    case 'A': return 1; 
    case 'C': return 2;
    case 'D': return 3;
    case 'E': return 4;
    case 'F': return 5;
    case 'G': return 6;
    case 'H': return 7;
    case 'I': return 8;
    case 'K': return 9;
    case 'L': return 10;
    case 'M': return 11;
    case 'N': return 12;
    case 'P': return 13;
    case 'Q': return 14;
    case 'R': return 15;
    case 'S': return 16;
    case 'T': return 17;
    case 'V': return 18;
    case 'W': return 19;
    case 'Y': return 20;
  }
  return 0;
}
char numberToAA(unsigned int AA){

  switch(AA){
  case 0: return '-';
  case 1: return 'A';
  case 2: return 'C';
  case 3: return 'D';
  case 4: return 'E';
  case 5: return 'F';
  case 6: return 'G';
  case 7: return 'H';
  case 8: return 'I';
  case 9: return 'K';
  case 10: return 'L';
  case 11: return 'M';
  case 12: return 'N';
  case 13: return 'P';
  case 14: return 'Q';
  case 15: return 'R';
  case 16: return 'S';
  case 17: return 'T';
  case 18: return 'V';
  case 19: return 'W';
  case 20: return 'Y';
  }
  return 0;


}

void writePairedFasta(unsigned int nPairs, MSA msa1, MSA msa2, unsigned int N1, unsigned int N2,
		      Ids ids1, Ids ids2, double* pairs,std::vector<std::pair<unsigned int,double> > &scores,
		      std::string fileName){

  std::ofstream out(fileName+"_pairedMSA.fasta");
  std::ofstream outIds(fileName+"_pairedIds.ids");
  std::string header;
  unsigned int p1,p2;
  for(unsigned int n=0;n<nPairs;n++){
    p1=pairs[3*scores[n].first];
    p2=pairs[3*scores[n].first+1];
    header=">"+ids1[p1]+"__"+ids2[p2];
    out<<header<<std::endl;
    outIds<<ids1[p1]+"  "+ids2[p2]<<std::endl;
    for(unsigned int i=0;i<N1;i++)
      out<<numberToAA(msa1[p1*N1+i]);
    for(unsigned int j=0;j<N2;j++)
      out<<numberToAA(msa2[p2*N2+j]);
    out<<std::endl;
  }
  out.close();
  outIds.close();
}
