#include "InputHandler.h"
#include "IPA.h"

using namespace std;


int main(int argc,char** argv){

  cout<<"Iterative Paralog Matching (Based on Bitbol et al.,PNAS 2016)"<<endl;
  cout<<"-------------------------------------------------------------"<<endl;
  double globalTimer=omp_get_wtime();
  //Parse input and load input data
  Parameters params=parseInput(argc,argv);
  omp_set_num_threads(params.nThreads);
  
  unsigned int N1,N2,B1,B2;
  Ids ids1;
  Ids ids2;
  MSA msa1=readFasta(params.msa1File,N1,B1,ids1);
  MSA msa2=readFasta(params.msa2File,N2,B2,ids2);
  Organisms os1=readOrganisms(params.os1File);
  Organisms os2=readOrganisms(params.os2File);
  unsigned int B1init=B1;
  unsigned int B2init=B2;
  unsigned int Nos1Init=unique(os1).size();
  unsigned int Nos2Init=unique(os2).size();
  
  // Check validity of input data
  if(B1!=os1.size() || B2!=os2.size()){
    cout<<"ERROR: Inconsistent number of sequence and organisms."<<endl;
    cout<<"B1="<<B1<<" , Nos1="<<os1.size()<<endl;
    cout<<"B2="<<B2<<" , Nos2="<<os2.size()<<endl;
    exit(1);
  }

  //Preprocesss data: Keep only shared organisms and group by organism 
  Organisms sharedOrgs=filterBySharedOrganisms(msa1,msa2,os1,os2,N1,N2,B1,B2,ids1,ids2);

  OrgsIndexes idx1=getOrgsIndexes(os1,sharedOrgs);
  OrgsIndexes idx2=getOrgsIndexes(os2,sharedOrgs);  
  cout<<"Dataset Statistics:"<<endl;
  cout<<"   MSA1: N1="<<N1<<" , B1="<<B1init<<" , NOrgs1="<<Nos1Init
      <<" , B1_shared="<<B1<<" , <m>="<<(double)B1/unique(os1).size()<<endl;
  cout<<"   MSA2: N2="<<N2<<" , B2="<<B2init<<" , NOrgs2="<<Nos2Init
      <<" , B2_shared="<<B2<<" , <m>="<<(double)B2/unique(os2).size()<<endl;
  cout<<"   Number of shared Organisms="<<unique(os1).size()<<endl;
  cout<<"   Output Prefix: "<<params.outFile<<std::endl;
  cout<<"   Number of threads: "<<params.nThreads<<std::endl;
  cout<<"   Random Seed: "<<params.seed<<std::endl;
  cout<<"-------------------------------------------------------------"<<endl;
  ofstream log(params.outFile+".log");
  log<<"Dataset Statistics:"<<endl;
  log<<"   MSA1: N1="<<N1<<" , B1="<<B1init<<" , NOrgs1="<<Nos1Init
      <<" , B1_shared="<<B1<<" , <m>="<<(double)B1/unique(os1).size()<<endl;
  log<<"   MSA2: N2="<<N2<<" , B2="<<B2init<<" , NOrgs2="<<Nos2Init
      <<" , B2_shared="<<B2<<" , <m>="<<(double)B2/unique(os2).size()<<endl;
  log<<"   Number of shared Organisms="<<unique(os1).size()<<endl;
  log<<"   Output Prefix: "<<params.outFile<<std::endl;
  log<<"   Number of threads: "<<params.nThreads<<std::endl;
  log<<"   Random Seed: "<<params.seed<<std::endl;
  log<<"-------------------------------------------------------------"<<endl;

  
  //Build initial random seed or load user defined seed
  unsigned int Bseed=0,N;
  MSA currentMSA;
  Ids seedIds;
  if(params.seedFile.compare(""))
    currentMSA=readFasta(params.seedFile,N,Bseed,seedIds);
  else
    currentMSA=buildRandomSeed(sharedOrgs,msa1,msa2,N1,N2,idx1,idx2,Bseed,params.seed);

  //Run main IPA algorithm 
  runIPA(params.Ninc,currentMSA,Bseed,msa1,msa2,N1,N2,B1,B2,idx1,idx2,ids1,ids2,sharedOrgs,params.outFile);

  cout<<"-------------------------------------------------------------"<<endl;
  cout<<"Finished in "<<omp_get_wtime()-globalTimer<<" s."<<std::endl;

  log<<"-------------------------------------------------------------"<<endl;
  log<<"Finished in "<<omp_get_wtime()-globalTimer<<" s."<<std::endl;
  log.close();
  return 0;
}
