#include "IPA.h"

void runIPA(unsigned  int Nincrement, MSA seedMSA,unsigned int B,
	    MSA msa1, MSA msa2,unsigned int N1, unsigned int N2,
	    unsigned int B1, unsigned int B2, OrgsIndexes idx1, OrgsIndexes idx2,Ids ids1, Ids ids2,
	    Organisms sharedOrgs,std::string OutputPrefix){


  //Count the maximal number of paired sequneces (nPairs)
  unsigned int nPairs=0;
  unsigned int* pairIndexes=new unsigned int[sharedOrgs.size()];
  for(unsigned int org=0;org<sharedOrgs.size();org++){
    pairIndexes[org]=nPairs;
    nPairs+=std::min(idx1[org][1]-idx1[org][0]+1,idx2[org][1]-idx2[org][0]+1);
  }
  double* pairs=new double[3*nPairs];
  double* weights=new double[nPairs];
  
  unsigned int N=N1+N2;
  double* J= new double[N*N*21*21];
  double* Cred=new double[N*N*20*20];

  unsigned int  currentB=B;
  MSA currentMSA=new unsigned int[N*nPairs];
  for(unsigned int b=0;b<currentB;b++)
    for(unsigned int i=0;i<N;i++)
      currentMSA[b*N+i]=seedMSA[b*N+i];
  delete[] seedMSA;
  
  unsigned int Nsteps=Nincrement;
  std::vector<std::pair<unsigned int,double> > scores;  

  std::ofstream log(OutputPrefix+".log",std::ofstream::app);
  //Main IPA loop, iteratively build matched MSA
  double timer;
  do{  
    //Build regularized covariance matrix
    timer=omp_get_wtime();
    buildCovarianceMatrix(Cred, currentMSA,currentB,N,0.5,0.9,weights);

    //Compute Js and shift to Ising gauge
    computeCouplingsInIsingGauge(J,Cred,N);

    //Evaluate the matching scores for all sequence-pairs
    computeMatchingScores(J,pairs,pairIndexes, msa1,msa2,N1,N2,idx1,idx2,sharedOrgs);

    //Select best matching pairs and rebuild current MSA
    scores=buildNextIterationMSA(Nincrement,pairs,nPairs,msa1,msa2,N1,N2,currentMSA);
    currentB=Nincrement;
    Nincrement+=Nsteps;
    
    std::cout<<"Progress: "<<currentB<<"/"<<nPairs<<" pairs matched. Iteration time: "
	     <<omp_get_wtime()-timer<<" s."<<std::endl<<std::flush;
    log<<"Progress: "<<currentB<<"/"<<nPairs<<" pairs matched. Iteration time: "
       <<omp_get_wtime()-timer<<" s."<<std::endl<<std::flush;
  }while(currentB<nPairs);
  

  //Print output
  writePairedFasta(nPairs,msa1,msa2,N1,N2,ids1,ids2,pairs,scores,OutputPrefix);
  
  //Clean Up
  delete[] J;
  delete[] Cred;
  delete[] weights;
  delete[] pairs;
  delete[] pairIndexes;
}

void buildCovarianceMatrix(double* C, MSA currentMSA,unsigned int B, unsigned int N,
			   double pseudoCounts, double reweightMaxId, double* weights){

  double timer=omp_get_wtime();
  unsigned int q=21;
  //Compute weights of the sequences
  double Beff=computeSeqIdWeights(weights,currentMSA,N,B,reweightMaxId);
  //Compute weighted frequencies and co-frequencies
  double* fi=new double[(q-1)*N];
  std::fill_n(C,(q-1)*(q-1)*N*N,0.);
  std::fill_n(fi,(q-1)*N,0.);

  for(unsigned int b=0;b<B;b++)
    for(unsigned int i=0;i<N;i++)
      if(currentMSA[b*N+i]>0)
	fi[(q-1)*i+currentMSA[b*N+i]-1]+=weights[b];

  for(unsigned int b=0;b<B;b++)
#pragma omp parallel for
    for(unsigned int i=0;i<N;i++)
      for(unsigned int j=i;j<N;j++)
	if(currentMSA[b*N+i]>0 && currentMSA[b*N+j]>0){
	  C[(q-1)*N*(((q-1)*i+currentMSA[b*N+i]-1)%((q-1)*N)) + ((q-1)*j+currentMSA[b*N+j]-1)]+=weights[b];
	  if(i!=j)
	    C[(q-1)*N*(((q-1)*j+currentMSA[b*N+j]-1)%((q-1)*N)) + ((q-1)*i+currentMSA[b*N+i]-1)]+=weights[b];
	}
  
  //Add pseudo-counts and compute covariance matrix
  for(unsigned int i=0;i<N;i++)
    for(unsigned int j=0;j<N;j++)
      for(unsigned int A=0;A<q-1;A++)
	for(unsigned int B=0;B<q-1;B++){
	  if(i!=j){
	    C[(q-1)*N*(((q-1)*i+A)%((q-1)*N)) + ((q-1)*j+B)]*=(1-pseudoCounts)/Beff;
	    C[(q-1)*N*(((q-1)*i+A)%((q-1)*N)) + ((q-1)*j+B)]+=pseudoCounts/(q*q)
	      -(((1-pseudoCounts)*fi[i*(q-1)+A]/Beff + pseudoCounts/q)*((1-pseudoCounts)*fi[j*(q-1)+B]/Beff + pseudoCounts/q));
	  }
	  else{
	    if(A==B){
	      C[(q-1)*N*(((q-1)*i+A)%((q-1)*N)) + ((q-1)*j+B)]*=(1-pseudoCounts)/Beff;
	      C[(q-1)*N*(((q-1)*i+A)%((q-1)*N)) + ((q-1)*j+B)]+=pseudoCounts/q
		-(((1-pseudoCounts)*fi[i*(q-1)+A]/Beff + pseudoCounts/q)*((1-pseudoCounts)*fi[j*(q-1)+B]/Beff + pseudoCounts/q));
	    }
	    else{
	      C[(q-1)*N*(((q-1)*i+A)%((q-1)*N)) + ((q-1)*j+B)]*=(1-pseudoCounts)/Beff;
	      C[(q-1)*N*(((q-1)*i+A)%((q-1)*N)) + ((q-1)*j+B)]-=
		(((1-pseudoCounts)*fi[i*(q-1)+A]/Beff + pseudoCounts/q)*((1-pseudoCounts)*fi[j*(q-1)+B]/Beff + pseudoCounts/q));
	    }
	  }
	}
  delete[] fi;
}

void computeCouplingsInIsingGauge(double* J,double* Cred,unsigned int N){

  unsigned int q=21;

  double timer=omp_get_wtime();
  
  //Explicitely compute Js in the zero-gap gauge
  int n=N*(q-1);
  
#if defined(__INTEL_COMPILER)
  // Use intel MKL routins to inverse the matrix using Cholesky decomposition
  int info;
  //Cholesky factorization
  dpotrf("L",&n,Cred,&n,&info); if(info != 0){std::cout << "c++ error: Cholesky failed" << std::endl;}
  //Compute inverse
  dpotri("L",&n,Cred,&n,&info);if(info != 0){std::cout << "c++ error: Cholesky inverse failed" << std::endl;}
#else
  //Use Alglib routines to inverse the matrix using Cholesky decomposition
  alglib::ae_int_t info;
  alglib::matinvreport rep;
  alglib::real_2d_array ag_C;
  ag_C.attach_to_ptr(n,n,Cred);
  spdmatrixinverse(ag_C, info, rep);
#endif

  
  //Shift the couplings to the zero-sum gauge
  double* mi=new double[q]; //<Jij(A,.)>
  double* mj=new double[q]; //<Jij(.,B)>
  double mm=0;              //<Jij(.,.)>
  std::fill_n(J,q*q*N*N,0.);
  for(unsigned int i=0;i<N;i++)
    for(unsigned int j=i+1;j<N;j++){
      std::fill_n(mi,q,0.);
      std::fill_n(mj,q,0.);
      mm=0;
      for(unsigned int A=0;A<q-1;A++)
        for(unsigned int B=0;B<q-1;B++){
	  mi[A]+=Cred[(q-1)*N*(((q-1)*i+A)%((q-1)*N)) + ((q-1)*j+B)]/q;
	  mj[A]+=Cred[(q-1)*N*(((q-1)*i+B)%((q-1)*N)) + ((q-1)*j+A)]/q;
	  mm+=Cred[(q-1)*N*(((q-1)*i+B)%((q-1)*N)) + ((q-1)*j+A)]/(q*q);
	}
      
      for(unsigned int A=0;A<q;A++)
	for(unsigned int B=0;B<q;B++){
	  if(A>0 && B>0)
	    J[q*N*((q*i+A)%(q*N)) + (q*j+B)]=Cred[(q-1)*N*(((q-1)*i+A-1)%((q-1)*N)) + ((q-1)*j+B-1)] -mi[A-1]-mj[B-1]+mm;
	  else if(A>0)
	    J[q*N*((q*i+A)%(q*N)) + (q*j+B)]= -mi[A-1]+mm;
	  else if (B>0)
	    J[q*N*((q*i+A)%(q*N)) + (q*j+B)]= -mj[B-1]+mm;
	  else
	    J[q*N*((q*i+A)%(q*N)) + (q*j+B)]= mm;
	}
    }  
}


MSA buildRandomSeed(Organisms sharedOrganisms,MSA msa1,MSA msa2, unsigned int N1,unsigned int N2,
		    OrgsIndexes idx1,OrgsIndexes idx2, unsigned int& Bseed, unsigned int rngSeed){

  std::vector<Sequence> tmpMSA;

  //For each shared organisms, randomly match sequences from the two MSAs
  unsigned nFam1,nFam2;
  if(!rngSeed)
    std::srand(unsigned(std::time(0)));
  else
    std::srand(rngSeed);
  for(unsigned int org=0;org<sharedOrganisms.size();org++){ 
    nFam1=idx1[org][1]-idx1[org][0]+1;
    nFam2=idx2[org][1]-idx2[org][0]+1;
    std::vector<unsigned int> range1(nFam1);
    std::iota(range1.begin(),range1.end(),idx1[org][0]);
    std::random_shuffle(range1.begin(),range1.end());
    std::vector<unsigned int> range2(nFam2);
    std::iota(range2.begin(),range2.end(),idx2[org][0]);
    std::random_shuffle(range2.begin(),range2.end());
    for(unsigned int b=0;b<std::min(nFam1,nFam2);b++){
      tmpMSA.push_back(Sequence(N1+N2));
      for(unsigned int i=0;i<N1;i++)
	tmpMSA[tmpMSA.size()-1][i]=msa1[range1[b]*N1+i];
      for(unsigned int i=0;i<N2;i++)
	tmpMSA[tmpMSA.size()-1][N1+i]=msa2[range2[b]*N2+i];
    }
  }

  //Convert MSAs to standard array form
  Bseed=tmpMSA.size();
  MSA msa = new unsigned int[(N1+N2)*tmpMSA.size()];
  for(unsigned int b=0;b<tmpMSA.size();b++)
    for(unsigned int i=0;i<N1+N2;i++)
      msa[b*(N1+N2)+i]=tmpMSA[b][i];

  return msa;
}

void computeMatchingScores(double* J, double* pairs,unsigned int* pairIndexes, MSA msa1, MSA msa2, unsigned int N1, unsigned int N2,
			   OrgsIndexes idx1, OrgsIndexes idx2,Organisms sharedOrganisms){

  unsigned nFam1,nFam2;
  unsigned q=21;
  unsigned int N=N1+N2;
  double E=0;
  std::vector<std::vector<double> > cutEs;
  unsigned int nPair=0;
#pragma omp parallel for private(E,cutEs,nPair,nFam1,nFam2) 
  for(unsigned int org=0;org<sharedOrganisms.size();org++){
    double timer=omp_get_wtime();
    nFam1=idx1[org][1]-idx1[org][0]+1;
    nFam2=idx2[org][1]-idx2[org][0]+1;
    //Compute interaction energies for all sequence pairs in current organism
    std::vector<std::vector<double> > Es;
    E=0;
    nPair=pairIndexes[org];
    for(unsigned int n1=0;n1<nFam1;n1++){
      Es.push_back(std::vector<double>(nFam2));
      for(unsigned int n2=0;n2<nFam2;n2++){
	E=0;
	for(unsigned int i=0;i<N1;i++)
	  for(unsigned int j=N1;j<N;j++)
	    E+=J[q*N*((q*i+msa1[(idx1[org][0]+n1)*N1+i])%(q*N)) + (q*j+msa2[(idx2[org][0]+n2)*N2+j-N1])];
	Es[n1][n2]=E;
      }
    }

    //Compute normalized scores
    cutEs=Es;
    unsigned int iMin,jMin,n1,n2;
    double Emin,Emin1,Emin2,score;
    double minScore=std::numeric_limits<double>::max();
    std::vector<unsigned int> toAssignI(nFam1);
    std::iota(toAssignI.begin(),toAssignI.end(),0);    
    std::vector<unsigned int> toAssignJ(nFam2);
    std::iota(toAssignJ.begin(),toAssignJ.end(),0);
    
    for(unsigned int k=0;k<std::min(nFam1,nFam2);k++){
      Emin=findMinEnergy(cutEs,iMin,jMin,Emin1,Emin2);
      getNLowerEnergies(Es,toAssignI[iMin],toAssignJ[jMin],n1,n2);
      score=std::min((Emin1-Emin)/(n1+1),(Emin2-Emin)/(n2+1));

      if(k<std::min(nFam1,nFam2)-1 && score<minScore)
	minScore=score;
      if(k==std::min(nFam1,nFam2)-1)
	score=minScore;
    
      //Consider border case: nFam1/nFam2=1
      if(nFam1==1 && nFam2==1)
	score=0;
      else if(nFam1==1)
	score=(Emin2-Emin)/(n2+1);
      else if(nFam2==1)
	score=(Emin1-Emin)/(n1+1);

      pairs[3*nPair]=idx1[org][0]+toAssignI[iMin];
      pairs[3*nPair+1]=idx2[org][0]+toAssignJ[jMin];
      pairs[3*nPair+2]=score;
      nPair++;
      toAssignI.erase(toAssignI.begin()+iMin);
      toAssignJ.erase(toAssignJ.begin()+jMin);
      cutEs.erase(cutEs.begin()+iMin);
      for(unsigned int n=0;n<cutEs.size();n++)
	cutEs[n].erase(cutEs[n].begin()+jMin);
    }

  } //End Loop On shared Organisms
}

double findMinEnergy(std::vector<std::vector<double> > Es,unsigned int &i,unsigned int &j, double &Emin1, double &Emin2){
  double Emin=Es[0][0]+1;
  double Emax=Es[0][0]-1;
  std::vector<double>::iterator tmpMin;
  std::vector<double>::iterator tmpMax;
  for(unsigned int n=0;n<Es.size();n++){
    tmpMin=std::min_element(Es[n].begin(),Es[n].end());
    tmpMax=std::max_element(Es[n].begin(),Es[n].end());
    if(*tmpMin<Emin){
      Emin=*tmpMin;
      i=n;
      j=tmpMin-Es[n].begin();
    }
    if(*tmpMax>Emax)
      Emax=*tmpMax;
  }
  Es[i][j]=Emax+1;
  Emin1=Emax+1;
  for(unsigned int n=0;n<Es.size();n++)
    if(Es[n][j]<Emin1)
      Emin1=Es[n][j];
  Emin2=*std::min_element(Es[i].begin(),Es[i].end());
  Es[i][j]=Emin;
  return Emin;
}

void getNLowerEnergies(std::vector<std::vector<double> > Es,unsigned int i, unsigned int j,
		       unsigned int &n1, unsigned int &n2){
  n1=0;
  n2=0;
  for(unsigned int n=0;n<Es.size();n++){
    if(Es[n][j]<Es[i][j])
      n1++;
  }
  for(unsigned int n=0;n<Es[0].size();n++){
    if(Es[i][n]<Es[i][j])
      n2++;
  }
}


std::vector<std::pair<unsigned int,double> > buildNextIterationMSA(unsigned int Nincrement,
								   double* pairs,unsigned int nPairs,MSA msa1,MSA msa2,
								   unsigned int N1, unsigned int N2, MSA currentMSA){
  //Sort scores in descending order
  std::vector<std::pair<unsigned int,double> > scores(nPairs);
  for(unsigned int i=0;i<nPairs;i++){
    scores[i].first=i;
    scores[i].second=pairs[3*i+2];
  }
  std::sort(scores.begin(),scores.end(),comparator);
    
  //Pairs seqs from msa1 and msa2 for the Nincrement best scoring pairs
  unsigned match1,match2;
  unsigned int N=N1+N2;
  for(unsigned int n=0;n<std::min(Nincrement,nPairs);n++){
    match1=pairs[3*scores[n].first];
    match2=pairs[3*scores[n].first+1];
    for(unsigned int i=0;i<N1;i++)
      currentMSA[n*N+i]=msa1[match1*N1+i];
    for(unsigned int j=0;j<N2;j++)
      currentMSA[n*N+j+N1]=msa2[match2*N2+j];
  }
  return scores;
}
