#include "InputHandler.h"

using namespace std;

Parameters parseInput(int argc, char** argv){

  //If help option passed display usage:
  if(argc==1 || !string(argv[1]).compare("-h") || !string(argv[1]).compare("--help")){
    cout<<"Usage: lbsDCA [options]"<<endl;
    cout<<"       -f1    : 1st Fasta file "<<endl;
    cout<<"       -f2    : 2st Fasta file "<<endl;
    cout<<"       -os1   : 1st Organism file "<<endl;
    cout<<"       -os2   : 2st Organism file "<<endl;
    cout<<"       -s     : Random number seed [default time] "<<endl;
    cout<<"       -Ni    : N increment [default 6] "<<endl;
    cout<<"       -seedMSA  : Seed of matched MSA [default none]"<<endl;
    cout<<"       -t     : Number of threads [default 4]"<<endl;
    cout<<"       -o     : Output Prefix [default \"output\"]"<<endl;
    exit(0);
}

  Parameters params;
  //Define default parameters
  params.Ninc=6;
  params.msa1File="";
  params.msa2File="";
  params.os1File="";
  params.os2File="";
  params.seed=0;
  params.seedFile="";
  params.nThreads=4;
  params.outFile="output";
    
  //Parse input arguments
  for(int i=0;i<argc;i++){
    if(!string(argv[i]).compare("-f1")){
      params.msa1File=string(argv[i+1]);
      i++;
    }
    if(!string(argv[i]).compare("-f2")){
      params.msa2File=string(argv[i+1]);
      i++;
    }
    if(!string(argv[i]).compare("-os1")){
      params.os1File=string(argv[i+1]);
      i++;
    }
    if(!string(argv[i]).compare("-os2")){
      params.os2File=string(argv[i+1]);
      i++;
    }
    if(!string(argv[i]).compare("-Ni")){
      params.Ninc=atoi(argv[i+1]);
      i++;
    }
    if(!string(argv[i]).compare("-s")){
      params.seed=atoi(argv[i+1]);
      i++;
    }
    if(!string(argv[i]).compare("-seedMSA")){
      params.seedFile=string(argv[i+1]);
      i++;
    }
    if(!string(argv[i]).compare("-t")){
      params.nThreads=atoi(argv[i+1]);
      i++;
    }
    if(!string(argv[i]).compare("-o")){
      params.outFile=string(argv[i+1]);
      i++;
    }
  }

  if(!params.msa1File.compare("") || !params.msa2File.compare("")){
    cout<<"Missing MSA file argument"<<endl;
    exit(1);
  }
  else if(!params.os1File.compare("") || !params.os2File.compare("")){
    cout<<"Missing Organism file argument"<<endl;
    exit(1);
  }
  return params;
}


double computeSeqIdWeights(double* weights, MSA msa, unsigned int N, unsigned int B, double maxId){
  double Beff=0;
  bool minDiffPassed=false;
  unsigned int diffCount=0;
  std::fill_n(weights,B,1.);
  unsigned int nDiff=(unsigned int)std::round((1-maxId)*N);
  for(unsigned int b=0;b<B-1;b++){
    for(unsigned int bp=b+1;bp<B;bp++){    
      minDiffPassed=false;
      diffCount=0;    
      for(unsigned int i=0;i<N;i++){
	diffCount+=(msa[b*N+i]!=msa[bp*N+i]);
	if(diffCount>=nDiff){
	  minDiffPassed=true;
	  break;
	}
      }
      if(!minDiffPassed){
	weights[b]++;
	weights[bp]++;
      }
    }
  }

  for(unsigned int b=0;b<B;b++){
    weights[b]=1/weights[b];
    Beff+=weights[b];
  }
  return Beff;
}


Organisms readOrganisms(std::string organismFile){
  Organisms orgs;
  ifstream inFile(organismFile.c_str());
  std::string buffer;
  while(getline(inFile,buffer))
      orgs.push_back(buffer);

  inFile.close();

  //If last read line is empty, remove
  string s=orgs[orgs.size()-1];
  unsigned int cnt=0;
  for(unsigned int i=0;i<s.size();i++)
    if(isspace(s[i]))
      cnt++;
  if(cnt==s.size())
    orgs.pop_back();

 
  return orgs;
}

Organisms filterBySharedOrganisms(MSA &msa1,MSA &msa2, Organisms &os1, Organisms &os2,
				  unsigned int N1,unsigned int N2,unsigned int &B1, unsigned int &B2,
				  Ids &ids1, Ids &ids2){

  Organisms uniOs1=unique(os1);
  Organisms uniOs2=unique(os2);
  Organisms sharedOrganisms;

  //Find all organisms shared by the two MSAs
  std::set_intersection(uniOs1.begin(),uniOs1.end(),
			uniOs2.begin(),uniOs2.end(),
			std::back_inserter(sharedOrganisms));

  
  //Extract sequences of shared organisms in MSAs 1 and 2
  std::vector<Sequence> tmpMSA1;
  std::vector<Sequence> tmpMSA2;
  Organisms tmpOs1, tmpOs2;
  Ids tmpIds1, tmpIds2;

  for(unsigned int o=0;o<sharedOrganisms.size();o++){
    for(unsigned int k1=0;k1<os1.size();k1++){
      if(!sharedOrganisms[o].compare(os1[k1])){
	tmpOs1.push_back(sharedOrganisms[o]);
	tmpMSA1.push_back(Sequence());
	tmpIds1.push_back(ids1[k1]);
	for(unsigned int i=0;i<N1;i++){
	  tmpMSA1[tmpMSA1.size()-1].push_back(msa1[k1*N1+i]);
	}
      }
    }
    for(unsigned int k2=0;k2<os2.size();k2++){
      if(!sharedOrganisms[o].compare(os2[k2])){
	tmpOs2.push_back(sharedOrganisms[o]);
	tmpMSA2.push_back(Sequence());
	tmpIds2.push_back(ids2[k2]);
	for(unsigned int i=0;i<N2;i++){
	  tmpMSA2[tmpMSA2.size()-1].push_back(msa2[k2*N2+i]);
	}
      }
    }
  }

  //Recast in standard array form
  delete[] msa1;
  delete[] msa2;
  B1=tmpOs1.size();
  B2=tmpOs2.size();
  msa1=new unsigned int[B1*N1];
  msa2=new unsigned int[B2*N2];
  os1=tmpOs1;
  os2=tmpOs2;
  ids1=tmpIds1;
  ids2=tmpIds2;
  for(unsigned int k1=0;k1<B1;k1++)
    for(unsigned int i=0;i<N1;i++)
      msa1[k1*N1+i]=tmpMSA1[k1][i];
  for(unsigned int k2=0;k2<B2;k2++)
    for(unsigned int i=0;i<N2;i++)
      msa2[k2*N2+i]=tmpMSA2[k2][i];

  //Return a vector of unique shared organisms
  return sharedOrganisms;

}

OrgsIndexes getOrgsIndexes(Organisms orgs,Organisms sharedOrgs){

  OrgsIndexes idx;
  idx.push_back(std::vector<unsigned int>(2));
  idx[0][0]=0;
  unsigned int orgCounter=0;
  for(unsigned int i=0;i<sharedOrgs.size();i++){
    while(orgCounter<orgs.size() && !orgs[orgCounter].compare(sharedOrgs[i])){
      orgCounter++;
    }
    idx[i][1]=orgCounter-1;
    idx.push_back(std::vector<unsigned int>(2));
    idx[i+1][0]=orgCounter;
  }
  idx.pop_back();
  return idx;
}


